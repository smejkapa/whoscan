# WhoScan WoW addon and tools #

About the project

## WoW Addon ##

## Convert and copy tools ##

### Prerequisities ###
* [Python 3](https://www.python.org/downloads/)
* `pip install requests`

### How to use it ###
1. From `Tools/Tools/` copy `post_data.py` to `<your_wow_dir>/WTF/`
1. Generate data via the addon
1. Make sure the server app is running
1. Run `python <your_wow_dir>/WTF/post_data.py`
1. Your data has been added to the database

**To see command line arguments run `post_data.py --help`**