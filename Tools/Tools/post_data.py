import os
import os.path
import time
import requests

uploadUri = "http://192.168.0.189/whoscan/upload.php"
postKey = "file"
dataFileName = "WhoScan.lua"

archiveDir = "WhoScanArchive"
if not os.path.exists(archiveDir):
    os.makedirs(archiveDir)


def convert(inputfile, outputfile):
    with open(inputfile) as input:
        with open(outputfile, "w") as output:
            for line in input:
                # Output only lines with data
                if not ";" in line:
                    continue
            
                # Reformat line
                data = line.split('"')[1].strip()
            
                # Output line
                output.write("{}\n".format(data))


fileProcessed = False
for dirpath, dirnames, filenames in os.walk("."):
    for filename in [f for f in filenames if f == dataFileName]:
        fileProcessed = True
        file = os.path.join(dirpath, filename)
        outFile = "{}.csv".format(file)
        print("Converting {}".format(file))
        convert(file, outFile)
        with open(outFile) as f:
            print("POSTing {}".format(file))
            r = requests.post(uploadUri, data={postKey: f.read()})
            print("{} {} {}".format(r.reason, r.status_code, r.content))
        os.remove(outFile)
        os.rename(file, os.path.join(archiveDir, "{}.lua.bak".format(str(time.time()))))
        print("")
        
if not fileProcessed:
    print("No files called '{}' to process found.".format(dataFileName))
else:
    print("All files processed and archived to '{}'".format(archiveDir))